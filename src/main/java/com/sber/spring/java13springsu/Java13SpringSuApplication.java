package com.sber.spring.java13springsu;

import com.sber.spring.java13springsu.dbexample.dao.BookDaoBean;
import com.sber.spring.java13springsu.dbexample.dao.BookDaoJDBC;
import com.sber.spring.java13springsu.dbexample.dao.UsersDaoBean;
import com.sber.spring.java13springsu.dbexample.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Date;
import java.util.List;

@SpringBootApplication
public class Java13SpringSuApplication
	implements CommandLineRunner {

	//------Надстройка над JDBC упрощающая жизнь!!!------
//	private NamedParameterJdbcTemplate jdbcTemplate;
//	@Autowired
//	public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
//		this.jdbcTemplate = jdbcTemplate;
//	}
	//---------------------------------------------------
	//****** UsersDaoBean ******

	private BookDaoBean bookDaoBean;

    @Autowired
    public void setBookDaoBean(BookDaoBean bookDaoBean) {
        this.bookDaoBean = bookDaoBean;
    }

//    @Autowired
//    private NamedParameterJdbcTemplate jdbcTemplate;

//    public Java13SpringProjectApplication(BookDaoBean bookDaoBean) {
//        this.bookDaoBean = bookDaoBean;
//    }

	private UsersDaoBean usersDaoBean;
	/*
	СОЗДАЕМ Bean нашего класса UsersDaoBean
	 */
	@Autowired
	public void setUsersDaoBean(UsersDaoBean usersDaoBean) {
		this.usersDaoBean = usersDaoBean;
	}

	public static void main(String[] args) {
		SpringApplication.run(Java13SpringSuApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//		BookDaoJDBC bookDaoJDBC = new BookDaoJDBC();
//		bookDaoJDBC.findBookById(1);
//
//        ApplicationContext ctx = new AnnotationConfigApplicationContext(MyDBConfigContext.class);
//        BookDaoBean bookDaoBean = ctx.getBean(BookDaoBean.class);
//        bookDaoBean.findBookById(1);
//        bookDaoBean.findBookById(1);
//
//        List<Book> books = jdbcTemplate.query("select * from books_dz6",
//                                              (rs, rowNum) -> new Book(
//                                                    rs.getInt("id"),
//                                                    rs.getString("title"),
//                                                    rs.getString("author"),
//                                                    rs.getDate("date_added")
//                                              ));
//        books.forEach(System.out::println);

/******************Домашнее задание № 6*************************
Вызываем метод, который позволяет добавить пользователей в базу:
*/

	usersDaoBean.addNewUser("Игнатенко",
			"Филипп",
			"1991-10-24",
			"88003231115",
			"fill_ignat@mail.ru",
			"Доктор Живаго; Сестра моя - жизнь; Путешествие из Петербурга в Москву");


/*
4. Написать метод, который принимает телефон/почту (на ваше усмотрение), достанет из
UserDAO список названий книг данного человека. С этим списком сходить в BookDAO и
получить всю информацию об этих книгах
*/

		// Вызываем метод getAllBooksClient, который печатает все книги согласно SQL запросу
		// передав в него номер телефона по средствам метода setUserPhone
		bookDaoBean.getAllBooksClient(usersDaoBean.setUserPhone("88003030330"));

	}
}
