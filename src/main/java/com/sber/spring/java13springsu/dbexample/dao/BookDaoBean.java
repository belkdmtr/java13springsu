package com.sber.spring.java13springsu.dbexample.dao;

import com.sber.spring.java13springsu.dbexample.model.Book;
import org.springframework.stereotype.Component;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Component
public class BookDaoBean {
    
    private final Connection connection;
    
    public BookDaoBean(Connection connection) {
        this.connection = connection;
    }
    
//    public Book findBookById(Integer bookId) throws SQLException {
//        PreparedStatement selectQuery = connection.prepareStatement("select * from books_dz6 where id = ?");
//        selectQuery.setInt(1, bookId);
//         /*
//         ResultSet интерфейс, который обработает и выполнит наш запрос SQL и в resultSet будет хранится наш набор данных
//         */
//        ResultSet resultSet = selectQuery.executeQuery();
//        Book book = new Book();
//        while (resultSet.next()) {
//            book.setBookAuthor(resultSet.getString("author"));
//            book.setBookTitle(resultSet.getString("title"));
//            book.setDateAdded(resultSet.getDate("date_added"));
//            System.out.println(book);
//        }
//        return book;
//    }

//****************************************************************************************
//ЗАДАЧА 4.2 С этим списком сходить в BookDAO и полуполучить всю информацию об этих книгах

    public void getAllBooksClient(Set<String> setBooksUsers) throws SQLException {
        //Первый цикл - пробегаемся по интерфейсу Set, так как он список
        //Второй цикл - пробегаемся по полученной строке из Set и разбиваем ее на отдельные книги, при помощи .split
        Book book = new Book();

        for (String setString : setBooksUsers) {
            for (String nameOfBook :setString.split("; ")) {
                PreparedStatement selectQuery = connection.prepareStatement("select * from books_dz6 where title = ?");
                selectQuery.setString(1, nameOfBook);

                ResultSet resultSet = selectQuery.executeQuery();

                while (resultSet.next()) {
                    book.setBookId(resultSet.getInt("id"));
                    book.setBookAuthor(resultSet.getString("author"));
                    book.setBookTitle(resultSet.getString("title"));
                    book.setDateAdded(resultSet.getDate("date_added"));
                    System.out.println(book);
                }
            }
        }
    }
}
