package com.sber.spring.java13springsu.dbexample.constants;

public interface DBConsts {
    String DB_HOST = "localhost";
    String DB = "local_db";
    String USER = "postgres";
    String PASSWORD = "12345";
    String PORT = "5432";
}
