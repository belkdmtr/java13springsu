/*
 ############ЗАДАЧА № 1
 1. Создать таблицу клиента с полями:
● Фамилия
● Имя
● Дата рождения
● Телефон
● Почта
● Список названий книг из библиотеки
 */

-- Создание таблицы клиентов
create table if not exists clients
(
    client_id            bigserial primary key,
    client_lastname      varchar(100) not null, -- Фамилия
    client_name          varchar(100) not null, -- Имя
    client_date_of_birth date,                  -- Дата рождения
    client_phone         varchar(11),           -- Телефон
    client_email         varchar(50),           -- Почта
    client_book          varchar(150) not null  -- Список названий книг из библиотеки
);

-- Заполнение таблицы клиентов предварительно для опытов!

insert into clients_dz6(client_lastname, client_name, client_date_of_birth, client_phone, client_email, client_book)
values ('Белкин', 'Дмитрий', (date '1983-07-06'), 88007070770, 'aka_diamond@mail.ru', 'Недоросль; Доктор Живаго; Сестра моя - жизнь');

insert into clients_dz6(client_lastname, client_name, client_date_of_birth, client_phone, client_email, client_book)
values ('Сидоров', 'Вениамин', (date '1999-12-22'), 88002020220, 'sidor40point@gmail.com', 'Путешествие из Петербурга в Москву');

insert into clients_dz6(client_lastname, client_name, client_date_of_birth, client_phone, client_email, client_book)
values ('Иванова', 'Роза', (date '2001-02-26'), 88003030330, 'roza_memoza@list.ru', 'Сестра моя - жизнь; Недоросль');

select client_book
from clients_dz6
where client_phone = '88007070770';









