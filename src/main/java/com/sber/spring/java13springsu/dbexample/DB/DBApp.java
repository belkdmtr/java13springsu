package com.sber.spring.java13springsu.dbexample.DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.sber.spring.java13springsu.dbexample.constants.DBConsts.*;

public enum DBApp {
    INSTANCE;
    
    private Connection connection;
    
    public Connection newConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB,
                                           USER, PASSWORD);
    }
}
