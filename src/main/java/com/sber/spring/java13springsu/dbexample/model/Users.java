package com.sber.spring.java13springsu.dbexample.model;

import lombok.*;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor //Конструктор для всех параметров класса
@ToString
public class Users {

    @Setter(AccessLevel.NONE)
    private Integer client_id;
    private String client_lastname;
    private String client_name;
    private Date client_date_of_birth;
    private String client_phone;
    private String client_email;
    private String client_book;

}
