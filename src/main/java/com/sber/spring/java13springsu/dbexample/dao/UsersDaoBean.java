/*
ЗАДАЧА № 2
Создать класс UserDAO и Бин (prototype) этого класса.
 */

package com.sber.spring.java13springsu.dbexample.dao;

import com.sber.spring.java13springsu.dbexample.model.Users;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;


/*
// Что бы к нам пришел наш connection из контекста (из класса MyDBConfigContext),
мы должны пометить его анотацией @Component

Класс UsersDaoBean забинен.
 */
@Component // UsersDaoBean является аннтоцией @Component и уже при запуске Спринга находится в его контексте
public class UsersDaoBean {
    private final Connection connection;

    // Внутри нашего класса UsersDaoBean есть инжект коннекшена
    public UsersDaoBean(Connection connection) {
        this.connection = connection;
    }

/*
ЗАДАЧА № 3
Далее нам необходимо заполнить пользователей (т.е. у нас должен быть метод в коде,
который позволяет добавить пользователей в базу)
*/
    public void addNewUser(String client_lastname, String client_name, String client_date_of_birth,
                           String client_phone, String client_email, String client_book) throws SQLException {

        //Создаем заранее подготовленное выражение SQL
        PreparedStatement insertQuery = connection.prepareStatement("insert into clients_dz6(client_lastname, client_name," +
                "client_date_of_birth, client_phone, client_email, client_book) values (?, ?, ?, ?, ?, ?)");
        insertQuery.setString(1, client_lastname);  // Выстави пжл параметр Стринговый к нашему запросу в индексе № 1
        insertQuery.setString(2, client_name);      // Выстави пжл параметр Стринговый к нашему запросу в индексе № 2
        insertQuery.setDate(3, java.sql.Date.valueOf(client_date_of_birth));   // Установим дату рождения, для начала переведем из String в Date
        insertQuery.setString(4, client_phone);
        insertQuery.setString(5, client_email);
        insertQuery.setString(6, client_book);

        //LocalDate пробовали?

        int rows = insertQuery.executeUpdate();
        //Users users = new Users();

        System.out.println("Добавлено " + rows + " строк в БД clients");

    }

/*
ЗАДАЧА 4.
НапиСоздать метод, который принимает телефон/почту (на ваше усмотрение), достанет из
UserDAO список названий книг данного человека. С этим списком сходить в BookDAO и
полуполучить всю информацию об этих книгах
*/

    public Set setUserPhone(String client_phone) throws SQLException {

        PreparedStatement selectQuery = connection.prepareStatement("select client_book " +
                "from clients_dz6 where client_phone = ?");
        selectQuery.setString(1, client_phone);

        ResultSet resultSet = selectQuery.executeQuery();

        //Поместим данные из поля таблицы client_book в интрфейс Set
        Set<String> setBooksUsers = new HashSet<>();

        while (resultSet.next()) {
            setBooksUsers.add(resultSet.getString("client_book"));

        }
       return setBooksUsers;
    }
}
