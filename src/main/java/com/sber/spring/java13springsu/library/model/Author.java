package com.sber.spring.java13springsu.library.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;


@Entity
@Table(name = "author")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "author_seq", allocationSize = 1)
public class Author
        extends GenericModel{

    @Column(name = "fio")
    private String fio;

    @Column(name = "description")
    private String description;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    //Связь мэни то мэни
    @ManyToMany(mappedBy = "authors")
    private Set<Book> books;
    //select * from authors join books

}



