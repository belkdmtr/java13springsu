package com.sber.spring.java13springsu.library.model;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Table(name = "book_rent_info")
@NoArgsConstructor
@Setter
@Getter
@SequenceGenerator(name = "default_gen", sequenceName = "book_rent_info_seq", allocationSize = 1)
public class BookRentInfo
        extends GenericModel {

    @ManyToOne
    @JoinColumn(name = "book_id", foreignKey = @ForeignKey(name = "FK_RENT_BOOK_INFO_BOOKS"))
    private Book book;

    @ManyToOne
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_RENT_BOOK_INFO_USERS"))
    private User user;

    @Column(name = "rent_date")
    private LocalDateTime rentDate;

    //поле автоматически должно рассчитываться из rent_date + rent_period
    @Column(name = "return_date")
    private LocalDateTime returnDate;

    //rent_period - количество дней аренды, если не указано, то по-умолчанию - 14 дней
    @Column(name = "rent_period")
    private Integer rentPeriod;

    @Column(name = "returned")
    private Boolean returned;
}
